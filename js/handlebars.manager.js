Handlebars.registerHelper('renderComments', function(comments, block) {
    var html = '';

    if (!comments || comments.length <= 0) {
        return '<h4>None comment yet. :(</h4>';
    }

    for (var i = 0; i < comments.length; ++i) {
        html += block.fn(comments[i]);

        if (i < (comments.length - 1)) {
            html += ' <hr> '
        }
    }
    return html;
});

Handlebars.registerHelper('renderProducts', function(products, block) {
    var html = '';

    for (var i = 0; i < products.length; ++i) {
        html += block.fn(products[i]);

        if (i < (products.length - 1)) {
            html += ' <hr> '
        }
    }
    return html;
});