(function ($, Handlebars, catalogueService) {
    var productId = getParameterByName('productId');
    var page = 1;
    var rowsPerPage = 3;

    if (!productId)
        window.location = '/';

    $.get('../templete/header.html', function (source) {
        $("#navigation").html(source);
    });

    $.get('../templete/product-detail.html', function (source) {
        catalogueService.getProduct(productId, function (product) {
            catalogueService.getCategory(product.categoryId, function (category) {
                product.categoryName = category.name;
                var templete = Handlebars.compile(source);
                var html = templete(product);

                $("#product-details").html(html);

                $("#product-comments").click(function () {
                    var productId = +$(this).attr('data-id');
                    var productName = $(this).attr('data-name');
                    catalogueService.getProductComments(productId, page, rowsPerPage, function (comments) {
                        var model = {
                            productName: productName,
                            comments: comments["responseJSON"] != null ? comments.responseJSON : comments
                        };

                        $.get('../templete/product-comment.html', function (source) {
                            var templete = Handlebars.compile(source);
                            var html = templete(model);
                            $('#modal-product-comments').append(html);
                            $('#product-detail-modal').modal('show');
                            $('#product-detail-modal').on('hidden.bs.modal', function () {
                                $('#modal-product-comments').html('');
                                page = 1;
                            });

                            if (page >= comments.totalPages || !comments.totalPages) {
                                $('.more-comments').attr('disabled', true);
                            }

                            $('.more-comments').click(function () {
                                page++;
                                $.get('../templete/comments.html', function (commentTemplete) {
                                    catalogueService.getProductComments(productId, page, rowsPerPage, function (moreComments) {
                                        var others = Handlebars.compile(commentTemplete);
                                        var html = others(moreComments);
                                        $('#comments').append('<hr>' + html);
                                        if (page >= moreComments.totalPages || !moreComments.totalPages) {
                                            $('.more-comments').attr('disabled', true);
                                        }
                                    });
                                });
                            });

                            $("#new-comment").click(function () {
                                $.get('../templete/new-comment.html', function (source) {
                                    $("#modal-new-comment").append(source);
                                    $("#add-comment").modal('show');
                                    $('#add-comment').on('hidden.bs.modal', function () {
                                        $('#modal-new-comment').html('');
                                    });

                                    $("#save-new-comment").click(function () {
                                        var model = {
                                            title: $("#title").val(),
                                            name: $("#author").val(),
                                            comment: $("#comment").val()
                                        }

                                        catalogueService.postComment(productId, model, function (res) {
                                            $('#add-comment').modal('hide');
                                            $('#add-comment').on('hidden.bs.modal', function () {
                                                $('#modal-new-comment').html('');
                                                $('#product-detail-modal').modal('hide');
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
} ($, Handlebars, window.Services.CatalogueService))