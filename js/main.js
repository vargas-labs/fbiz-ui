(function($, Handlebars, catalogueService) {
    var page = 1;
    var rowsPerPage = 5;
    var categoryId = -1;

    if (getParameterByName('categoryId'))
        categoryId = +getParameterByName('categoryId');


    $.get('/templete/header.html', function(source) {
        $("#navigation").html(source);
    });

    catalogueService.getProducts(page, rowsPerPage, categoryId, function(products) {
        renderProducts(products);
    });

    var renderProducts = function(products) {
        $.get('/templete/products.html', function(source) {
            $("#products").html('');
            var templete = Handlebars.compile(source);
            var html = templete(products);
            $("#products").html(html);
            $("#more-products").click(function() {
                page++;
                catalogueService.getProducts(page, rowsPerPage, categoryId, function(products) {
                    var source = "{{#renderProducts this}}<a href='/details?productId={{id}}'>{{name}}</a> {{/renderProducts}}";
                    var templete = Handlebars.compile(source);
                    var html = templete(products.data);
                    $('.panel-body').html($('.panel-body').html() + '<hr>' + html);

                    if (page >= products.totalPages) {
                        $("#more-products").attr('disabled', 'true');
                    }
                });
            });

            renderCategories();
        });
    }

    var renderCategories = function() {
        $.get('/templete/category.html', function(source) {
            catalogueService.getCategories(function(categories) {
                var templete = Handlebars.compile(source);
                var html = templete(categories);
                $("#categories-dropdown").html(html);

                $('.category').click(function() {
                    categoryId = +$(this).attr('data-id');
                    page = 1;

                    catalogueService.getProducts(page, rowsPerPage, categoryId, function(products) {
                        renderProducts(products);
                    });
                });
            });
        });
    }

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

} ($, Handlebars, window.Services.CatalogueService));