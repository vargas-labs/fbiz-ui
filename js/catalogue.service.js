var CatalogueService = {
    getProducts: function (page, rowsPerPage, categoryId, callback) {
        $.get(this.apiEndpoint + '/product?page=' + page + '&rowsPerPage=' + rowsPerPage + '&categoryId=' + categoryId, function (response) {
            callback(response);
        });
    },
    getProductsByCategory: function (categoryId, callback) {
        $.get(this.apiEndpoint + '/category/' + categoryId + '/products', function (response) {
            callback(response);
        });
    },
    getProductComments: function (productId, page, rowsPerPage, callback) {
        $.ajax({
            url: this.apiEndpoint + '/product/' + productId + '/comments?page=' + page + '&rowsPerPage=' + rowsPerPage,
            type: 'GET',
            error: function (error) {
                callback(error);
            },
            success: function (data) {
                callback(data);
            }
        });
    },
    getCategories: function (callback) {
        $.get(this.apiEndpoint + '/category', function (response) {
            callback(response);
        });
    },
    getCategory: function (categoryId, callback) {
        $.get(this.apiEndpoint + '/category/' + categoryId, function (response) {
            callback(response);
        });
    },
    getProduct: function (productId, callback) {
        $.get(this.apiEndpoint + '/product/' + productId, function (response) {
            callback(response);
        });
    },
    postComment: function (productId, comment, callback) {
        $.ajax({
            url: this.apiEndpoint + '/product/' + productId + '/comments',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(comment),
            contentType: 'application/json',
            error: function (error) {
                callback(error);
            },
            success: function (data) {
                callback(data);
            }
        });
    },
    apiEndpoint: 'http://fbiz-api.azurewebsites.net/api'
}

window.Services = window.Services || {};
window.Services.CatalogueService = CatalogueService;